#!/usr/bin/env bash
PATH=$PATH:/usr/local/bin

main() {
    local bucket=$1
    local mount=$2
    local passwd=$3
    local url=$4

    # Each mount needs its own cache
    local cache=/tmp/s3fs_cache/$(basename $url)

    # Delete cache if exceed 5GB
    if [ -d "$cache" ] && (($(du -sm $cache | cut -f1) > 5120)); then
        echo "Removing the following s3fs cache: $cache"
        rm -rf $cache
    fi

    if mountpoint -q "$mount"; then
        echo "$mount is already mounted"
    else
        echo "$mount is not already mounted, mounting it..."
        s3fs $bucket $mount -o passwd_file=$passwd -o url=$url -o use_cache=$cache -o allow_other -o use_path_request_style -o uid=1000 -o gid=1000
    fi
}

main $1 $2 $3 $4
