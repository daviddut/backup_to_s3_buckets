#!/usr/bin/env bash
PATH=$PATH:/usr/local/bin

SAVE_DESTINATION=$1

for backup in "${@:2}"; do
    echo "Pulling ${backup} rsync initiated to the following save destination: $SAVE_DESTINATION"
    rsync -azhe ssh --delete "$backup" "$SAVE_DESTINATION"

    if [ "$?" -eq "0" ]; then
        echo "Pulling ${backup} rsync completed."
    else
        echo "Error! There was a failure during backup rsyncing operation!"
        exit 1
    fi
done
