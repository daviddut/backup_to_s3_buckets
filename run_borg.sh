#!/usr/bin/env bash
PATH=$PATH:/usr/local/bin

main() {
    local borg_repo=$1
    local archive_to_backup=$2
    local borg_passphrase=$3
    local keep_within=$4

    export BORG_PASSPHRASE=$borg_passphrase

    # some helpers and error handling:
    info() { printf "\n%s %s\n\n" "$(date)" "$*" >&2; }
    trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

    info "Starting backup on repository: $borg_repo and for archive: $archive_to_backup"

    # Backup into an archive named after the machine this script is currently running on.
    # --show-rc: show return code
    borg create \
        --verbose \
        --stats \
        --show-rc \
        --compression lz4 \
        --exclude-caches \
        $borg_repo::'{hostname}-{now}' \
        $archive_to_backup

    backup_exit=$?

    # limit prune's operation to this machine's archives
    borg prune \
        --list \
        --prefix '{hostname}-' \
        --show-rc \
        --keep-within $keep_within \
        $borg_repo

    prune_exit=$?

    # use highest exit code as global exit code
    global_exit=$((backup_exit > prune_exit ? backup_exit : prune_exit))

    if [ ${global_exit} -eq 1 ]; then
        info "Backup and/or Prune finished with a warning"
    fi

    if [ ${global_exit} -gt 1 ]; then
        info "Backup and/or Prune finished with an error"
    fi

    return ${global_exit}
}

main $1 $2 $3 $4
