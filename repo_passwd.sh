#!/usr/bin/env bash
PATH=$PATH:/usr/local/bin

# IMPORTANT: This is only a sample config file
#
# 1. Copy this file to:  ${HOME}/.credentials/repo_passwd.sh
# 2. Enter your credentials

S3_SPACES_BORG_PASSPHRASE='test123'
S3_WASABI_BORG_PASSPHRASE='test123'
