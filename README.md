# Backup script documentation

## Initialize a Borg repository with encryption
borg init --encryption=repokey borg

## Set up Borg password passphrase for backup script with repo_passwd.sh

**IMPORTANT: repo_passwd.sh is only a sample config file and is not meant to be edited in its current location.**

Instruction to set up password passphrase:

 1. Copy repo_passwd.sh to: ${HOME}/.credentials/repo_passwd.sh
 2. Enter your credentials
