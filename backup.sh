#!/usr/bin/env bash
PATH=$PATH:/usr/local/bin

. ${HOME}/.credentials/repo_passwd.sh

# Remote ssh source to rsync locally
REMOTE_BACKUPS=(
    ''
    ''
)


# Rsync destinationt/backup
ARCHIVE_DESTINATION='/backups'

BACKUP_REPOSITORIES="${HOME}/backup_repositories"

# Examples:
S3_MOUNT[0]="${BACKUP_REPOSITORIES}/s3_spaces"
S3_BUCKET[0]="bucket_name"
S3_URL[0]="https://sfo2.digitaloceanspaces.com"
S3_PASSWD[0]="${HOME}/.credentials/passwd-s3fs-spaces"

S3_MOUNT[1]="$backup{BACKUP_REPOSITORIES}/s3_wasabi"
S3_BUCKET[1]="bucket_name"
S3_URL[1]="https://s3.wasabisys.com"
S3_PASSWD[1]="${HOME}/.credentials/passwd-s3fs-wasabi"

PRUNE_KEEP_WITHIN_5_YEARS='1825d'
PRUNE_KEEP_WITHIN_10_YEARS='3650d'

BORG_SOURCE[0]=$ARCHIVE_DESTINATION
BORG_REPO[0]=${S3_MOUNT[0]}
BORG_PASSWD[0]=$S3_SPACES_BORG_PASSPHRASE
BORG_PUNE_KEEP_WITHIN[0]=$PRUNE_KEEP_WITHIN_5_YEARS

BORG_SOURCE[1]=$ARCHIVE_DESTINATION
BORG_REPO[1]=${S3_MOUNT[1]}
BORG_PASSWD[1]=$S3_WASABI_BORG_PASSPHRASE
BORG_PUNE_KEEP_WITHIN[1]=$PRUNE_KEEP_WITHIN_10_YEARS

main() {
    # Mount S3 buckets
    for i in "${!S3_MOUNT[@]}"; do
        ./mount_s3fs.sh ${S3_BUCKET[$i]} ${S3_MOUNT[$i]} ${S3_PASSWD[$i]} ${S3_URL[$i]}
    done

    # Rsync backups to local destination
    ./pull_backup.sh "${ARCHIVE_DESTINATION}" "${REMOTE_BACKUPS[@]}"

    if [ "$?" -eq "0" ]; then
        local global_exit=0

        # Backup to borg repositories
        for i in "${!BORG_REPO[@]}"; do
            ./run_borg.sh ${BORG_REPO[$i]}"/borg" ${BORG_SOURCE[$i]} ${BORG_PASSWD[$i]} ${BORG_PUNE_KEEP_WITHIN[$i]}

            local ret_borg=$?

            global_exit=$((ret_borg > global_exit ? ret_borg : global_exit))
        done

        exit ${global_exit}
    else
        echo "Error! Skipping borg backup because rsync operation has failed!"
        exit 1
    fi
}

# Start backup
main
